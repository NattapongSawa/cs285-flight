//Reset input when click back page
$(window).bind("pageshow", () => {
	var form = $('form');
	form[0].reset();
});
var flightModule = angular.module('flightModule',[]);
flightModule.service('myService',function(){});
flightModule.controller('searchCtrl',function($scope,$window,$http,myService){
	$scope.nextPage = function(){
		if(validateFrom()){
			localStorage.setItem('flyingFrom', $("#flyingFrom").val())
            localStorage.setItem('flyingTo', $("#flyingTo").val())
			localStorage.setItem('departing', $('#departing').val())
            localStorage.setItem('returning', $('#returning').val())
            localStorage.setItem('type', $('input[name=optradio]:checked', '#searchflight').val())
            localStorage.setItem('adults', $('#adults').val())
			localStorage.setItem('cabin', $('#cabin').val())
			$window.location.href = "DisplayFlight.html";
		}
		else{
			$window.alert("กรุณากรอกข้อมูลให้ครบ");
		}
	};
	$(document).ready(() => {
		$("#flyingFrom").change(() => {
			$('#departing option').each(function () {
				$(this).remove();
			});
			$.each(allDeparting, (index, item) => {
				if ($("#flyingFrom").val() === item.city) {
					$.each(item.airport, (key, airport) => {
						$('#departing').append(`<option value="${key}">${airport}</option>`)
					})
				}
			})
		})
		$('#flyingTo').change(() => {
			$('#returning option').each(function () {
				$(this).remove();
			});
			$.each(allDeparting, (index, item) => {
				if ($("#flyingTo").val() === item.city) {
					$.each(item.airport, (key, airport) => {
						$('#returning').append(`<option value="${key}">${airport}</option>`)
					})
				}
			})
		})
	})
});
function validateFrom(){
	var input1 = document.getElementById("flyingFrom").value;
	var input2 = document.getElementById("flyingTo").value;
	if((input1=="")||(input2=="")){
		return false;
	}
	else {
		return true;
	}
}


