const express = require('express')
const jsonQuery = require('json-query')
const app = express()
const port = 3000;

var bodyParser = require('body-parser')
var cors = require('cors')
var data = require('./Data')

app.use(cors())
app.use(bodyParser.json())
// post/search   get/price  calculateTax    post/Detail

app.post('/search', function (req, res) {

    var query = '[*fromAirport='+req.body.departing+'][*toAirport='+req.body.returning+']'
    var result = jsonQuery(query, {
        data: data.allFlights
    })
    console.log("query data: "+query)
    console.log("result: "+result.value)
    res.send(result.value)
})

app.post('/detail', function (req, res) {
    //waiting
    var query = '[noFlight='+req.body.noFlight+']'
    var result = jsonQuery(query, {
        data: data.allFlights
    })
    console.log("query data: "+query)
    console.log("result: "+result.value)
    res.send(result.value)
})

app.post('/calculate', (req, res) => {
    var tax = 1000; // 1000/flight
    var total = req.body.price + tax + req.body.service + req.body.insurance;
    var vat =   total * 0.07;    //7%
    var totalNvat = total + vat;
    sendData ={
        tax:vat,
        total:totalNvat
    }
    console.log("total amount: "+ totalNvat)
    res.send(sendData)
})


app.listen(port, () => console.log(`App listening on port 3000!`))