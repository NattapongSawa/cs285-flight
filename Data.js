var allDeparting = [
    {
        city: "Bangkok",
        airport: {
            BKK: "Suvarnabumi (BKK)",
            DMK: "Donmueng (DMK)"
        }
    },
    {
        city: "Hongkong",
        airport: {
            HKA: "Hong Kong International Airport (HKA)"
        }
    },
    {
        city: "Singapore",
        airport: { 
            SIA: "Singapore Air (SIA)" 
        }
    },
    {
        city: "Paris",
        airport: { 
            CDG: "charles de gaulle (CDG)" 
        }
    }
]

var allFlights = [
    {
        noFlight: 2501,
        price: 3519,
        airline: "Singapore Air",
        fromAirport: "BKK",
        toAirport: "CDG",
        takeoffTime: "19:05",
        landingTime: "06:50",
        travelHours: "17h 45m",
        transitNumber: 2,
        transits: [
            {
                transitAirport: "SIN",
                transitCountry: "Singapore",
                transitHours: 2.15
            },
            {
                transitAirport: "MUC",
                transitCountry: "Germany",
                transitHours: 1.30
            }
        ]
    },
    {
        noFlight: 2602,
        price: 2055,
        airline: "EI AI Israel Air",
        fromAirport: "BKK",
        toAirport: "CDG",
        takeoffTime: "15:35",
        landingTime: "05:30",
        travelHours: "19h 55m",
        transitNumber: 2,
        transits: [
            {
                transitAirport: "SIN",
                transitCountry: "Singapore",
                transitHours: 2.15
            },
            {
                transitAirport: "MUC",
                transitCountry: "Germany",
                transitHours: 1.30
            }
        ]
    },
    {
        noFlight: 4333,
        price: 3519,
        airline: "Singapore Air",
        fromAirport: "BKK",
        toAirport: "CDG",
        takeoffTime: "16:40",
        landingTime: "06:50",
        travelHours: "20h 10m",
        transitNumber: 2,
        transits: [
            {
                transitAirport: "SIN",
                transitCountry: "Singapore",
                transitHours: 2.15
            },
            {
                transitAirport: "MUC",
                transitCountry: "Germany",
                transitHours: 1.30
            }
        ]
    },
    {
        noFlight: 8244,
        price: 8888,
        airline: "Bangkok Air",
        fromAirport: "BKK",
        toAirport: "HKA",
        takeoffTime: "00:00",
        landingTime: "07:50",
        travelHours: "7h 50m",
        transitNumber: 2,
        transits: [
            {
                transitAirport: "SIN",
                transitCountry: "Singapore",
                transitHours: 2.15
            },
            {
                transitAirport: "MUC",
                transitCountry: "Germany",
                transitHours: 1.30
            }
        ]
    },
]

exports.allDeparting = allDeparting;
exports.allFlights = allFlights;